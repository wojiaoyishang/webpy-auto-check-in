<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

// 引导用户
if (!check_login()){header("Location: login_page.php");}


// post请求 操作任务
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	// 修改操作
	if ($_POST['action'] == 'edit'){
		$data = array();
		foreach($_POST as $key => $value){
			$data[$key] = trim($_POST[$key]);
		}
		sql_query("UPDATE `webpy_part` SET `userdata`='" . encode_string(json_encode($data,JSON_UNESCAPED_UNICODE)) . "' , `partcalled` = '" . $_POST['partcalled'] . "' WHERE `partcard` = '" . $_POST['partcard'] . "' AND `username` = '" . $user_name . "'");
		
		echo '成功保存了修改！';
		die();
	}
	
	// 增加操作
	if ($_POST['action'] == 'AddPart'){
		if (in_array($_POST['partname'],array("Acfun","BiliBili","iqiyi","wyy","QQmusic","ydybj","jrtt")) == false) die();
		if (sql_query("select * FROM webpy_part where username = '" . $user_name . "' AND partname = '" . $_POST['partname'] . "'")[0] >= 3 and $user_level == 1){
			echo '无法添加新任务！因为您当前的用户组一个项目只能添加3个任务！';
			die();
		}
		$srting = get_randomstr();
		while (sql_query("select * FROM webpy_part where partcard = '" . $srting . "'")[0] != 0){
			$srting = get_randomstr();
		}
		sql_query(sprintf("INSERT INTO `webpy_part` (`username`,`partname`,`regtime`,`partcard`) VALUES ('%s','%s','%s','%s');",$user_name,$_POST['partname'],date("Y-m-d H:i:s"),$srting));
		die();
	}
	// 删除操作
	if ($_POST['action'] == 'DelPart'){
		sql_query(sprintf("DELETE FROM `webpy_part` WHERE `username`='%s' AND `partcard` = '%s'",$user_name,$_POST['partcard']));
		die();
	}

}
?>

<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Python自动化程序管理后台主页 - 以赏的秘密小屋</title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">
	
	<link rel="shortcut icon" href="<?php echo $web_icon;?>" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<style>body{margin: 20px 20px 20px 20px;min-width: 1600px;}div{width : 100%;max-width: 1600px;}button.btn.btn-primary {margin-top: 10px;}span.badge.badge-info {margin-top: 12px;}label {margin-top: 5px;}.card-body {margin: 15px 15px 15px 15px;}.tab-content {margin: 15px 15px 15px 15px;}.tabbable {width: 98%;}.form-control {width: 98%;margin-bottom: 15px;}.make-switch {width: 98%;margin-bottom: 15px;}</style>
  </head>
  
  <body>

    <div class="container-fluid">
	<div class="row-fluid">
		<div class="col-md-12">
			<div class="alert alert-dismissable alert-info">		
			<h4>公告</h4><?php echo $dashboard_notice ; //引入公告 ?></div>
			<ul class="nav nav-pills" style="margin-bottom: 15px;">
				<li class="nav-item"><a class="nav-link" href="./index.php">数据总览页</a></li>
				<li class="nav-item"><a class="nav-link" href="./log_page.php">日志查看页</a></li>
				<li class="nav-item"><a class="nav-link active" href="./setting_page.php">程序设置页</a></li>
				<li class="nav-item"><a class="nav-link" href="./user_page.php">用户信息页</a></li>
				<p class="nav-link"><?php echo "登录用户：" . $user_name ?></p>
				
				<li class="nav-item dropdown ml-md-auto">
					 <a class="nav-link dropdown-toggle"id="navbarDropdownMenuLink" data-toggle="dropdown">其它选项</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						 
						 <a class="dropdown-item" href="http://blog.zhangyishang.top">项目首页</a> 
						 <a class="dropdown-item" href="#">赞助开发者</a> 
						 <div class="dropdown-divider"></div> 
						 <a class="dropdown-item" href="javascript:logout()">退出登录</a>
						 
					</div>
				</li>
				
			</ul>
		
		
		
		
		
		
		
		
		
		<div class="container-fluid" style="margin-bottom: 15px;">
		<div>
		
		
		
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="Acfun" href="#Acfun">Acfun签到设置</a>
				</div>
				
				<div id="Acfun" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'Acfun'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="Acfun" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="Acfun">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>Acfun cookie：</p>
										<input type="text" class="form-control" name="cookie" value="<?php echo htmlspecialchars($userdata['cookie']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动领取每日香蕉</span></strong></a><br>
										<button type="submit" class="btn btn-primary" >保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="Acfun">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
			
			</div>
					
			
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="BiliBili" href="#BiliBili">BiliBili签到设置</a>
				</div>
				
				<div id="BiliBili" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'BiliBili'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="BiliBili" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="BiliBili">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>BiliBili cookie：</p>
										<input type="text" class="form-control" name="cookie" value="<?php echo htmlspecialchars($userdata['cookie']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动领取每日硬币，自动直播中心签到。</span></strong></a><br>
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="BiliBili">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
			
			</div>
			
			
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="iqiyi" href="#iqiyi">爱奇艺签到设置</a>
				</div>
				
				<div id="iqiyi" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'iqiyi'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="iqiyi" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="iqiyi">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>爱奇艺签到请求 URL：</p>
										<input type="text" class="form-control" name="url" value="<?php echo htmlspecialchars($userdata['url']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动签到。</span></strong></a><br>
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="iqiyi">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
			
			</div>
			
		
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="wyy" href="#wyy">网易云音乐签到设置</a>
				</div>
				
				<div id="wyy" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'wyy'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="wyy" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="wyy">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>网易云音乐手机号：</p>
										<input type="text" class="form-control" name="phone" value="<?php echo htmlspecialchars($userdata['phone']); ?>">
										<p>网易云音乐对应密码md5：</p>
										<input type="text" class="form-control" name="passmd5" value="<?php echo htmlspecialchars($userdata['passmd5']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动打卡签到，自动刷单。网易云登录向服务器发送md5，为了不收集直接的敏感信息请自行将密码加密成md5再填写，md5转化(32位大)-><a href="https://tool.chinaz.com/tools/md5.aspx">点这里</a><-</span></strong></a><br>
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="wyy">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
			</div>
				
			
			<!-- ... -->
			
			
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="QQmusic" href="#QQmusic">QQ音乐签到设置</a>
				</div>
				
				<div id="QQmusic" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'QQmusic'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="QQmusic" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="QQmusic">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>QQ音乐 - cookie：</p>
										<input type="text" class="form-control" name="cookie" value="<?php echo htmlspecialchars($userdata['cookie']); ?>">
										<!-- <p>QQ音乐 - sign：</p>
										<input type="text" class="form-control" name="sign" value="<?php echo htmlspecialchars($userdata['sign']); ?>"> -->
										<a><strong><span style="font-size:15px;">说明：自动领取每日积分。</span></strong></a><br>
										<!-- sign获取方法：在调试控制台输入getSecuritySign()获取（去掉引号） -->
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="QQmusic">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
			
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="ydybj" href="#ydybj">有道云笔记签到设置</a>
				</div>
				
				<div id="ydybj" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'ydybj'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="ydybj" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="ydybj">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>有道云 - cookie：</p>
										<input type="text" class="form-control" name="cookie" value="<?php echo htmlspecialchars($userdata['cookie']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动领取每日空间。cookie必须是手机版客户端登录的。</span></strong></a><br>
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="ydybj">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
			
			
			<div class="card">
			
				<div class="card-header">
					 <a class="card-link" data-toggle="collapse" data-parent="jrtt" href="#jrtt">今日头条签到设置</a>
				</div>
				
				<div id="jrtt" class="collapse show">
					<div class="card-body">	
						<div class="tabbable">
							<?php 
								$result = sql_query2("select * FROM webpy_part where username = '" . $user_name . "' and partname = 'jrtt'"); // 数据库结果
								$rows = $result[0];  // 数量
								$data = $result[1];  // 数据
								if ($rows == 0) echo '<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="jrtt" style="margin-bottom: 15px;">添加新任务</button>';
							?>
							<ul class="nav nav-tabs">
							<?php 
								for ($i=1;$i<=$rows;$i++) { ?>
									<li class="nav-item"><a class="nav-link<?php if ($i == 1) echo " active show"; ?>" href="#tab<?php echo $i; ?>" data-toggle="tab">任务<?php echo $i; ?></a></li>
							<?php } ?>
							</ul>
							
					
							
							<div class="tab-content">
								<?php
									for ($i=1;$i<=$rows;$i++) {
										$userdata = json_decode(decode_string($data[$i - 1]['userdata']), true); // 用户保存	
								?>
								<div class="tab-pane<?php if ($i == 1) echo " active"; ?>"  id="tab<?php echo $i; ?>">
									<form method="post">
										<div class="make-switch" data-on="success" data-off="success"><input type="checkbox" name="state" <?php if ($userdata['state'] == 'on') echo 'checked'; ?>> 启用此任务</div>
										<input type="hidden" class="form-control" name="action" value="edit">
										<input type="hidden" class="form-control" name="partname" value="jrtt">
										<input type="hidden" class="form-control" name="partcard" value="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">
										<p>任务ID（系统自动生成）：<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?></p>
										<p>显示名称：</p>
										<input type="text" class="form-control" name="partcalled" value="<?php echo htmlspecialchars($userdata['partcalled']); ?>">
										<p>今日头条 - cookie：</p>
										<input type="text" class="form-control" name="cookie" value="<?php echo htmlspecialchars($userdata['cookie']); ?>">
										<a><strong><span style="font-size:15px;">说明：自动领取每日金币。</span></strong></a><br>
										<button type="submit" class="btn btn-primary">保存更改</button>
									</form>
									<button type="button" class="btn btn-success btn-sm" id="PartADD" data-partname="jrtt">添加新任务</button>
									<button type="button" class="btn btn-danger btn-sm" id="PartDEL" data-partcard="<?php echo htmlspecialchars($data[$i - 1]['partcard']); ?>">删除此任务</button>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>	
				</div>
				
			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		
		</div>
		</div>
		
		
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		</div>
	</div>
	</div>

	
	
	
	
	
	
	
	
	
	
	
	

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script>
	var msg = <?php echo '"' . $msg . '"' ?>;
	function logout(){document.location.href = 'login_page.php?action=logout'}

	
	$('button').click(function(){
		
		
		if (this.type == "submit"){

			var targetUrl = $(this.form).attr("action");
			var data =  $(this.form).serialize();
			$.ajax({
				type: 'post',
				url: targetUrl,
				cache: false,
				data: data, //重点必须为一个变量如：data
				success: function (data) {
					alert(data)
				},
				error: function () {
					alert('保存失败，请重试！')
				}
				
			})
			
			return false;
		}
		
		if (this.id == 'PartADD'){
			$.ajax({
			  type: 'POST',
			  url: 'setting_page.php',
			  data: {"action":"AddPart","partname":this.dataset.partname},
			  success: function(rs){
				  if (rs != ""){alert(rs);}
				  location.reload();
			  }
			});
		}
		
		
		if (this.id == 'PartDEL'){
			$.ajax({
			  type: 'POST',
			  url: 'setting_page.php',
			  data: {"action":"DelPart","partcard":this.dataset.partcard},
			  success: function(rs){
				  if (rs != ""){alert(rs);}
				  location.reload();
			  }
			});
		}
		
	})
	</script>
  </body>
</html>
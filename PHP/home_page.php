<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

// 引导用户
if (!check_login()){header("Location: login_page.php");}

// 用户POST请求查看数据

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	// 查询详情
	


}
?>

<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Python自动化程序管理后台主页 - 以赏的秘密小屋</title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">
	
	<link rel="shortcut icon" href="<?php echo $web_icon;?>" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	<style>body{margin: 20px 20px 20px 20px;min-width: 1600px;} div{width : 100%;max-width: 1600px;}.modal-backdrop.fade.show{margin: 0 auto;}</style>
  </head>
  
  <body>

    <div class="container-fluid">
	<div class="row-fluid">
		<div class="col-md-12">
			<div class="alert alert-dismissable alert-info">		
			<h4>公告</h4><?php echo $dashboard_notice ; //引入公告 ?></div>
			<ul class="nav nav-pills" style="margin-bottom: 15px;">
				<li class="nav-item"><a class="nav-link active" href="./index.php">数据总览页</a></li>
				<li class="nav-item"><a class="nav-link" href="./log_page.php">日志查看页</a></li>
				<li class="nav-item"><a class="nav-link" href="./setting_page.php">程序设置页</a></li>
				<li class="nav-item"><a class="nav-link" href="./user_page.php">用户信息页</a></li>
				<p class="nav-link"><?php echo "登录用户：" . $user_name ?></p>
				
				<li class="nav-item dropdown ml-md-auto">
					 <a class="nav-link dropdown-toggle"id="navbarDropdownMenuLink" data-toggle="dropdown">其它选项</a>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
						 
						 <a class="dropdown-item" href="http://blog.zhangyishang.top">项目首页</a> 
						 <a class="dropdown-item" href="#">赞助开发者</a> 
						 <div class="dropdown-divider"></div> 
						 <a class="dropdown-item" href="javascript:logout()">退出登录</a>
						 
					</div>
				</li>
				
			</ul>
			
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>项目名称</th>
						<?php $day = (int)$_GET['day']; // 正数后退
							for ($x=0; $x<=6; $x++) {echo "<th>" . date("Y/m/d",strtotime("-" . (string)(6 - $x + $day) . " day")) . "</th>";}?>
					</tr>
				</thead>
				<tbody>
					<?php
						
						$result = sql_query2('select * FROM webpy_part where username = "' . $user_name . '"'); // 先把所有结果取出来
						$amount = $result[0];  // 一共有多少个
						$dataAll = $result[1];   // 所有数据库数据

					?>
					
					<?php
					  for ($i=0;$i<$amount;$i++){
						  $userdata = json_decode(decode_string($dataAll[$i]['userdata']), true);
						  $returndata = json_decode(decode_string($dataAll[$i]['returndata']), true);
						  
						  if ($userdata['state'] != 'on'){
							  continue;
						  }
					?>
					
						<tr id="<?php echo $dataAll[$i]['partcard'] ?>">
							<td><?php echo $dataAll[$i]['partcalled'] ?></td>
							
							<?php 
							
								for ($x=0; $x<=6; $x++) { 
									$date = date("Y/m/d",strtotime("-" . (string)(6 - $x + $day) . 'day'));
									if ($returndata[$date][0] == ''){
										echo "<td>-</td>";
									}else{
										echo '<td><a href="#YishangLovesPikachu-' . $dataAll[$i]['partcard'] . '-' . (string)$x . '" data-toggle="modal">' . $returndata[$date][0] . '</a></td>';
									}
									if ($returndata[$date][1] == ""){continue;}
							?>
								<div class="modal fade" id="YishangLovesPikachu-<?php echo $dataAll[$i]['partcard'] . '-' . $x; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin:0 auto">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="myModalLabel">数据详情信息</h5> 
												<button type="button" class="close" data-dismiss="modal">
													<span aria-hidden="true">×</span>
												</button>
											</div>
											<div class="modal-body">
												<a>项目所处时间：<?php echo $date; ?></a><br>
												<a>数据所处项目名称：<?php echo $dataAll[$i]['partcalled']; ?></a><br>
												<a>数据详情：</a><br>
												<a><strong><span style="font-size:15px;word-break: break-all;"><?php echo $returndata[$date][1]; ?></span></strong></a>
											</div>
											<div class="modal-footer">
												 
												<button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</tr>
					  <?php } ?>
					
				</tbody>
			</table>
			<p><?php echo date("Y/m/d",strtotime("-" . (string)(6 - $day) . " day")); ?>  到  <?php echo date("Y/m/d",strtotime("-" . (string)($day) . " day")); ?></p>
			<nav>
				<ul class="pagination">
					<li class="page-item">
						<a class="page-link" href="./home_page.php?day=<?php echo $day + 7; ?>">上个星期</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="./home_page.php">回到今天</a>
					</li>
					<li class="page-item">
						<a class="page-link" href="./home_page.php?day=<?php echo $day - 7; ?>">下个星期</a>
					</li>
				</ul>
			</nav>
			
			
		</div>
		
	</div>
	</div>
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script>function logout(){document.location.href = 'login_page.php?action=logout'}</script>
  </body>
</html>
<?php
// 引入加载器
require_once dirname(__FILE__) . "/core/loader.php";

if (check_login()){header("Location: home_page.php");exit;}  // 登录有效

$warning = '';  // 注册提示
$active = false; // 是否处于激活

// get请求 激活
if ($_SERVER['REQUEST_METHOD'] == 'GET' and $_GET['action'] == 'active' and trim($_GET['code']) != ''){
	sql_query("UPDATE `webpy_user` SET `level`='1' WHERE `activecode` = '" . trim($_GET['code']) . "'");
	
	$warning = '您的账户激活成功！';
	$active = true;
}


// post请求 注册
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if (trim($_POST['username']) == '' or trim($_POST['email']) == '' or trim($_POST['password']) == ''){
		$warning = '用户名、邮箱或密码不能为空！';
	}elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
		$warning = '邮箱格式错误！';
	}elseif (preg_match('/^[A-Za-z0-9_\x{4e00}-\x{9fa5}]+$/u',trim($_POST['username'])) == false){
		$warning = '用户名由2-16位数字或字母、下划线组成！';
	}else{
		
		
		
		// 未激活或未创建
		if (sql_query("select * FROM webpy_user where username = '" . trim($_POST['username']) . "'")[0] == 0 or sql_query("select * FROM webpy_user where username = '" . trim($_POST['username']) . "'")[1]['level'] == 0){
			if (sql_query("select * FROM webpy_user where username = '" . trim($_POST['username']) . "'")[0] == 0){
				// 不存在就创建
				sql_query(sprintf("INSERT INTO `webpy_user` (`username`, `email`, `password`, `regtime`, `activecode`,`level`) VALUES ('%s', '%s', '%s', '%s', '%s','0');",trim($_POST['username']),trim($_POST['email']),md5(trim($_POST['password'])),date("Y-m-d H:i:s"),encode_string(trim($_POST['username'])."activecode")));
				if (sql_query("select * FROM webpy_log where username = '" . trim($_POST['username']) . "'")[0] == 0) sql_query("INSERT INTO `webpy_log` (`username`) VALUES ('" . trim($_POST['username']) . "');");
			}
			
			$warning = '我们向您的邮箱里发送了一封邮件，<br>请打开里面的网址来激活您的账户！';
			$active = true;
			
			$active_href = "http://" . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] . "?action=active&code=" . encode_string(trim($_POST['username'])."activecode");
			
			if (send_email(trim($_POST['email']),trim($_POST['username']),'激活您的Python自动化账户',
			"<h2>尊敬的用户 ". trim($_POST['username']) ." 在 ". $web_title ." 上注册了一个账户。</h2>
			<h2>若不是您本人的操作请忽略此邮件。</h2>
			<p>若您想要激活您的账户，请点击此链接来激活您的账户：<a href=\"" . $active_href . "\">" . $active_href . "</p>"
			) == false){
				$warning = '发送邮件失败！请联系网站管理员！';
			}
			
		}else{
			$warning = '此账号已被注册！请更换用户名再试！';
		}
	}
}
?>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>注册Python自动化程序管理后台用户 - <? echo $web_title; ?></title>

    <meta name="description" content="Python自动化程序">
    <meta name="author" content="我叫以赏">
	
	<link rel="shortcut icon" href="<?php echo $web_icon;?>" type="image/x-icon">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	

  </head>
  <body>

    <div class="container-fluid" style="width: 400px;box-shadow: 2px 2px 40px #00000024;border-radius: 10px;">
	<div class="row" style="margin: 10px 10px 10px 0px;margin-top: 30px;margin-bottom: 30px;">
		<div class="col-md-12" style="margin: 10px 10px 10px 0px;margin-top: 10%;margin-bottom: 30px;">
			<h3 class="text-center">用户注册</h3> 
			<p class="text-center" id="logtip"><?php echo $register_notice; ?></p>
			<p class="text-center"><span style="color:#E53333;"><strong><?php echo $warning; ?></strong></span></p>
			
			<?php if (!$active) { ?>
			<form method="post" name="register">
				<div class="form-group"> 
					<label for="Username_Table">用户名</label>
					<input type="text" class="form-control" name="username" placeholder="Enter your username ......">
				</div>
				<div class="form-group">
					 <label for="Email_Table">邮箱</label>
					 <input type="email" class="form-control" name="email" placeholder="Enter your email ......">
				</div> 
				<div class="form-group">
					 <label for="Password_Table">密码</label>
					 <input type="password" class="form-control" name="password" placeholder="Enter your password ......">
				</div> 
				<button class="btn btn-primary" >检查并注册</button>
				<a href="login_page.php"><button class="btn btn-outline-success" type="button">我已有账户</button></a>
			</form>
			<?php }else{ ?>
			<a href="login_page.php"><button class="btn btn-outline-success" type="button">我已经激活</button></a>
			<?php } ?>
		</div>
	</div>
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>